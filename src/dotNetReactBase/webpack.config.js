﻿var path = require('path');
var webpack = require('webpack');

module.exports = {
    entry: {
        server: './React/server.jsx',
        client: './React/client.jsx',
        clientWithRender: './React/clientWithRender.jsx',
    },
    output: { path: __dirname + '/wwwroot/js/', filename: 'react.[name].bundle.js' },
    module: {
        loaders: [
            {
                test: /.jsx?$/,
                loader: "babel-loader",
                exclude: /node_modules/,
                query: {
                    presets: ['es2015', 'react'],
                    plugins: ['react-html-attrs', 'transform-class-properties', 'transform-decorators-legacy']
                }
            }
        ]
    }
};
